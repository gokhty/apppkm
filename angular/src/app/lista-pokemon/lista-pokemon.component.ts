import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-pokemon',
  templateUrl: './lista-pokemon.component.html',
  styleUrls: ['./lista-pokemon.component.sass']
})
export class ListaPokemonComponent implements OnInit {
  public arr: Array<any> = [];
  constructor() { 
	
	fetch('https://pokemon-pichincha.herokuapp.com/pokemons/?idAuthor=1')
	.then(response => response.json())
    .then(json => this.arr = json)
	.catch(error => console.log("error al cargar datos"));
  }

  ngOnInit(): void {
	  console.log(this.arr);
  }

}
