import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-nuevo-pokemon',
  templateUrl: './nuevo-pokemon.component.html',
  styleUrls: ['./nuevo-pokemon.component.sass']
})
export class NuevoPokemonComponent implements OnInit {
  
  formPkm = new FormGroup({
	  name : new FormControl('')
  });
  
  constructor() { 
	
  }
  ngOnInit(): void {
  }
  
  submit1(): void {
	  var data = {
		  name:this.formPkm.value.name, 
		  attack:this.formPkm.value.attack,
		  image:this.formPkm.value.image,
		  defence:this.formPkm.value.defence
		  };
	  fetch('https://pokemon-pichincha.herokuapp.com/pokemons/?idAuthor=1',{
		  method: 'POST',
		  body: JSON.stringify(data),
		  headers:{
			  'Content-Type':'application/json'
		  }
	  }).then(res => res.json())
	  .catch(error => console.error("Error",error))
	  .then(response => console.log('Success: '));

  }
  
}
