import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListaPokemonComponent } from './lista-pokemon/lista-pokemon.component';
import { NuevoPokemonComponent } from './nuevo-pokemon/nuevo-pokemon.component';
@NgModule({
  declarations: [
    AppComponent,
    ListaPokemonComponent,
    NuevoPokemonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
